import frappe
from frappe import _
from erpnext.stock.report.stock_ledger.stock_ledger import get_item_group_condition
from .projected_qty import get_item_in_condition
from six import string_types
import json

def _get_columns():
    """return columns"""

    columns = [
        _("Item")+":Link/Item:100",
        _("Item Name")+"::374",
        _("Item Group")+":Link/Item Group:100",
        _("Brand")+":Link/Brand:90",
        _("Description")+"::140",
        _("Warehouse")+":Link/Warehouse:280",
        _("Stock UOM")+":Link/UOM:90",
        _("Opening Qty")+":Float:100",
        _("Opening Value")+":Float:110",
        _("In Qty")+":Float:80",
        _("In Value")+":Float:80",
        _("Out Qty")+":Float:80",
        _("Out Value")+":Float:80",
        _("Balance Qty")+":Float:100",
        _("Balance Value")+":Float:100",
        _("Valuation Rate")+":Float:90",
        _("Reorder Level")+":Float:80",
        _("Reorder Qty")+":Float:80",
        _("Company")+":Link/Company:100"
    ]

    return columns


def get_items(filters):
    conditions = []
    if filters.get("item_code"):
        conditions.append("item.name=%(item_code)s")
    else:
        if filters.get("item_group"):
            conditions.append(get_item_group_condition(filters.get("item_group")))
        item_filters_fields = ['brand', 'item_name', 'default_supplier', 'product_group', 'barcode', 'sortiment', 'asin', 'topseller_kategorie']
        for f in item_filters_fields:
            if filters.get(f):
                if f=="item_name":
                    conditions.append("item.item_name like %(item_name)s")
                    filters[f] = "%%%s%%" % filters[f]
                else:
                    conditions.append("item.{0}=%({0})s".format(f))

    items = []
    if conditions:
        items = frappe.db.sql_list("""select name from `tabItem` item where {}"""
            .format(" and ".join(conditions)), filters)
    return items



"""
For Stock Summary Report
"""
@frappe.whitelist()
def get_data(item_code=None, warehouse=None, item_group=None, filters=None,
    start=0, sort_by='actual_qty', sort_order='desc'):
    '''Return data to render the item dashboard'''
    conditions = []
    values = []
    if item_code:
        conditions.append('b.item_code=%s')
        values.append(item_code)
    elif filters:
        if isinstance(filters, string_types):
            filters = json.loads(filters)
        items = get_item_in_condition(filters)
        if len(items):
            conditions.append("b.item_code in ('{}')".format(u"', '".join(items).encode('utf-8').strip()))
    if warehouse:
        conditions.append('b.warehouse=%s')
        values.append(warehouse)
    if item_group:
        conditions.append('i.item_group=%s')
        values.append(item_group)

    if conditions:
        conditions = ' and ' + ' and '.join(conditions)
    else:
        conditions = ''

    return frappe.db.sql('''
    select
        b.item_code, b.warehouse, b.projected_qty, b.reserved_qty,
        b.reserved_qty_for_production, b.reserved_qty_for_sub_contract, b.actual_qty, b.valuation_rate, i.item_name
    from
        tabBin b, tabItem i
    where
        b.item_code = i.name
        and
        (b.projected_qty != 0 or b.reserved_qty != 0 or b.reserved_qty_for_production != 0 
        or b.reserved_qty_for_sub_contract != 0 or b.actual_qty != 0)
        {conditions}
    order by
        {sort_by} {sort_order}
    limit
        {start}, 21
    '''.format(conditions=conditions, sort_by=sort_by, sort_order=sort_order,
        start=start), values, as_dict=True)
