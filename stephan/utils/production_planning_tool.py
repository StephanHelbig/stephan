import frappe
from frappe.utils import flt


def _get_production_items(self):
    item_dict = {}
    for d in self.get("items"):
        item_details= {
            "production_item"		: d.item_code,
            "sales_order"			: d.sales_order,
            "material_request"		: d.material_request,
            "material_request_item"	: d.material_request_item,
            "bom_no"				: d.bom_no,
            "description"			: d.description,
            "stock_uom"				: d.stock_uom,
            "company"				: self.company,
            "wip_warehouse"			: "",
            "fg_warehouse"			: d.warehouse,
            "status"				: "Draft",
            "planned_start_date"      : d.planned_start_date,
            "planned_end_date"      : d.planned_end_date,
            "expected_delivery_date": d.expected_delivery_date,
            "project"				: frappe.db.get_value("Sales Order", d.sales_order, "project")
        }

        """ Club similar BOM and item for processing in case of Sales Orders """
        if self.get_items_from == "Material Request":
            item_details.update({
                "qty": d.planned_qty
            })
            item_dict[(d.item_code, d.material_request_item, d.warehouse)] = item_details

        else:
            item_details.update({
                "qty":flt(item_dict.get((d.item_code, d.sales_order, d.warehouse),{})
                    .get("qty")) + flt(d.planned_qty)
            })
            item_dict[(d.item_code, d.sales_order, d.warehouse)] = item_details

    return item_dict