from __future__ import unicode_literals
import frappe
from frappe.desk.query_report import get_script as original_get_acript
from frappe.model.utils import render_include


@frappe.whitelist()
def get_script(report_name):
    original_data = original_get_acript(report_name)
    extra_scrpts = []
    for app in frappe.get_installed_apps():
        hooks = frappe.get_hooks('report_js', app_name=app)
        if report_name in hooks:
            for script in hooks[report_name]:
                extra_scrpts.append(app + '/' + script)

    if not original_data.get('script'):
        original_data['script'] = ''
    includes = ''
    for url in extra_scrpts:
        includes += '{% include "'+url+'" %} \n'

    scripts = render_include(includes)

    original_data['script'] += scripts

    return original_data