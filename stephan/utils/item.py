import frappe

def validate(doc,method):
    if doc.lagerplatz:
        lagerplat = frappe.get_doc("Lagerplaetze",doc.lagerplatz)
        found_lag = 0
        for lag in lagerplat.lagerplatznr:
            if lag.item == doc.name:
                found_lag = 1
                break

        if not found_lag:
            lager = lagerplat.append('lagerplatznr', {})
            lager.item = doc.name
            lager.save()
    else:
        # for lags in frappe.db.sql("""SELECT parent FROM `tabLagerplatzNr` WHERE item=%s""",doc.name):
        frappe.db.sql("""DELETE FROM `tabLagerplatzNr` WHERE item=%s""",(doc.name))


def on_trash(doc,method):
    # if doc.lagerplatz:
    #     lagerplat = frappe.get_doc("Lagerplaetze",doc.lagerplatz)
    #     found_lag = 0
    #     for lag in lagerplat.lagerplatznr:
    #         if lag.item == doc.name:
    #             found_lag = 1
    #             break
    #
    #     if not found_lag:
    #         lager = lagerplat.append('lagerplatznr', {})
    #         lager.item = doc.name
    #         lager.save()
    # else:
    #     # for lags in frappe.db.sql("""SELECT parent FROM `tabLagerplatzNr` WHERE item=%s""",doc.name):
    frappe.db.sql("""DELETE FROM `tabLagerplatzNr` WHERE item=%s""",(doc.name))