// Copyright (c) 2019, Mainul Islam and contributors
// For license information, please see license.txt

frappe.ui.form.on('Download Template', {
	refresh: function(frm) {
		if (!frm.doc.__islocal)
			frm.events.make_select_options(frm);
	},
	make_select_options: function (frm) {
		frappe.model.with_doctype(frm.doc.ref_doctype, r => {
			const filter_fields = df => frappe.model.is_value_type(df) && !df.hidden;
			const get_fields = dt => frappe.meta.get_docfields(dt).filter(filter_fields);
			const selected_values = {}
			frm.doc.fields.forEach(f=>(
				selected_values[f.parent_field] = JSON.parse(f.field)
			))
			const doctype_fields = get_fields(frm.doc.ref_doctype)
				.map(df => ({
					label: df.label,
					value: df.fieldname,
					checked: (typeof selected_values['doctype_fields'] !== "undefined" && 
					selected_values['doctype_fields'].includes(df.fieldname)? 1:0)
				}));
			let fields = [
				{
					"label": frm.doc.ref_doctype,
					"fieldname": "doctype_fields",
					"fieldtype": "MultiCheck",
					"options": doctype_fields,
					"columns": 2,
					"hidden": 1
				}
			];

			const child_table_fields = frappe.meta.get_table_fields(frm.doc.ref_doctype)
				.map(df => {
					let fname = df.fieldname + '_fields';
					return {
						"label": df.options,
						"fieldname": fname,
						"fieldtype": "MultiCheck",
						"options": frappe.meta.get_docfields(df.options)
							.filter(filter_fields)
							.map(df => ({
								label: df.label,
								value: df.fieldname,
								checked: (typeof selected_values[fname] !== "undefined" && 
								selected_values[fname].includes(df.fieldname)? 1:0)
							})),
						"columns": 2,
						"hidden": 1
					};
				});

			fields = fields.concat(child_table_fields);
			frm.meta.fields.concat(fields)
			frm.fields_dict.field_wrapper.$wrapper.html('')
			fields.forEach(field=>{
				frm.fields_dict[field.fieldname] = frappe.ui.form.make_control({
					parent: frm.fields_dict.field_wrapper.$wrapper,
					df:field
				})
				frm.fields_dict[field.fieldname].refresh_input()
			})
		}, false)
	},
	before_save: function (frm) {
		frm.doc.fields = []
		$.each(frm.fields_dict, (i, f)=>{
			if (typeof f.df.fieldname !== "undefined" && f.df.fieldname.endsWith('_fields')) {
				let val = f.get_value()
				if (val.length) {
					frm.add_child('fields', {
						parent_ref_doctype: f.df.label,
						parent_field: f.df.fieldname,
						field: JSON.stringify(val)
					})
				}
			}
		})
	}
});
