# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "stephan"
app_title = "Stephan"
app_publisher = "Mainul Islam"
app_description = "Stephan"
app_icon = "fa fa-users"
app_color = "green"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

boot_session = "stephan.utils.boot.get_boot_info"

# include js, css files in header of desk.html
app_include_css = "/assets/css/stephan.css"
app_include_js = "/assets/js/stephan.js"

# include js, css files in header of web template
# web_include_css = "/assets/stephan/css/stephan.css"
# web_include_js = "/assets/stephan/js/stephan.js"

# include js in page
page_js = {
    "stock-balance": "public/js/reports/stock_summary.js"
}

# include js in doctype views
doctype_js = {
    "Item": "public/js/item.js",
    "Purchase Order": "public/js/purchase_order.js",
    "Production Order": "public/js/production_order.js",
    "Production Planning Tool": "public/js/production_planning_tool.js",
    "BOM": "public/js/bom.js",
    "Stock Entry": "public/js/stock_entry.js"
}
doctype_list_js = {
    "Item": "public/js/item_list.js",
    "BOM": "public/js/bom_list.js",
    "Purchase Order": "public/js/purchase_order_list.js",
    "Stock Entry": "public/js/stock_entry_list.js"
}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# include js for report
report_js = {
    "Stock Projected Qty": "public/js/reports/projected_qty.js",
    "Stock Balance": "public/js/reports/stock_balance.js",
    "Stock Ledger": "public/js/reports/stock_ledger.js"
}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "stephan.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "stephan.install.before_install"
after_install = "stephan.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "stephan.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
    "Purchase Order": {
        "validate": "stephan.utils.purchase_order.validate"
    },
    "Item": {
        "validate": "stephan.utils.item.validate",
        "on_trash": "stephan.utils.item.on_trash"
    }
}

# Scheduled Tasks
# ---------------

scheduler_events = {
    # "all": [
    # 	"stephan.tasks.all"
    # ],
    "daily_long": [
        "frappe.utils.backups.new_backup"
    ]
    # "hourly": [
    # 	"stephan.tasks.hourly"
    # ],
    # "weekly": [
    # 	"stephan.tasks.weekly"
    # ]
    # "monthly": [
    # 	"stephan.tasks.monthly"
    # ]
}

# Testing
# -------

# before_tests = "stephan.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
override_whitelisted_methods = {
    "frappe.desk.search.search_widget": "stephan.utils.search._search_widget",
    "frappe.desk.query_report.get_script": "stephan.utils.report_queries.get_script"
}

fixtures = ["Print Format", "Custom Field", "Property Setter"]
