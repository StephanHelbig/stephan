import frappe

def execute():
    frappe.reload_doctype('Item', True)
    frappe.db.sql_ddl("alter table tabItem modify google_drive_ref VARCHAR(999)")
    frappe.db.sql("""update tabItem set google_drive_ref=CONCAT("https://drive.google.com/drive/search?q=", artikel_nummer) where google_drive_ref is null or google_drive_ref=''""")