var add_summarized = 1;

frappe.ui.form.on("Stock Entry", {
        refresh: function(frm) {
        if (!frm.doc.__islocal) {
            frm.add_custom_button(__("Update Images"), function(){
                frm.events.update_bom_from_item(frm, {
                    'image':'image'
                })
            })
            frm.add_custom_button(__("Update Item Name"), function(){
                frm.events.update_bom_from_item(frm, {
                    'item_name':'item_name'
                })
            })
        }
    },
    update_bom_from_item(frm, fields) {
        frappe.call({
            method: "stephan.stephan.so.update_item",
            args: {
                bom: frm.doc.name,
                fields: fields
            },
            freeze: true,
            callback: r => {
                if(r['message']){
                    if(typeof r.message === "string") r.message = JSON.parse(r.message);
                    frappe.model.sync(r.message)
                    cur_frm.refresh()
                }
            }
        })
    },
    download_summarized:function (frm) {
              frappe.call({
				method: "stephan.stephan.so.download_table",
				args:{"docname":cur_frm.doc.name,"summarized":true},
				freeze: true,
                                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(cur_frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (cur_frm.fields_dict['download_link']) {
                            		$(cur_frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			});
    },
        download_excel:function (frm) {
              frappe.call({
				method: "stephan.stephan.so.download_table",
				args:{"docname":cur_frm.doc.name,"summarized":false},
				freeze: true,
                                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(cur_frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (cur_frm.fields_dict['download_link']) {
                            		$(cur_frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			});
    },
    onload: function(frm) {
        frm.set_indicator_formatter('item_code',
			function(doc) { 
            if (!doc.s_warehouse) {
                return 'blue';
            } else {
                return (doc.qty<=doc.actual_qty) ? "green" : "orange" 
            }
        }, function(doc){
            if (doc.image)
                return `<span class="avatar avatar-small"> <span class="avatar-frame" style="background-image: url(${doc.image})"></span></span> ${doc.item_code}`;
            return doc.item_code
        });


                if (add_summarized == 1) {
            let btn = document.createElement('a');
            btn.innerText = 'Download Summarized Supplied Items';
            btn.className = 'grid-summarized btn btn-xs btn-default';
            frm.fields_dict.items.grid.wrapper.find('.grid-upload').removeClass('hide').parent().append(btn);
            btn.addEventListener("click", function () {

                frappe.call({
				method: "stephan.stephan.po3.download_table",
				args:{"docname":cur_frm.doc.name},
				freeze: true,
                                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(cur_frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (cur_frm.fields_dict['download_link']) {
                            		$(cur_frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			})

            });


            //    let btn2 = document.createElement('a');
            // btn2.innerText = 'Download Supplied Items';
            // btn2.className = 'grid-supplied btn btn-xs btn-default';
            // this.frm.fields_dict.supplied_items.grid.wrapper.find('.grid-summarized').removeClass('hide').parent().append(btn2);
            // btn2.addEventListener("click", function () {
            //
            //            frappe.call({
				// method: "stephan.stephan.po.download_table",
				// args:{"docname":cur_frm.doc.name},
				// freeze: true,
            //                            callback: function (r) {
				//     console.log("DOWNLOAD BOMS AS PDF");
				// 		console.log(r);
				// 		if (r.message) {
				// 			$(cur_frm.fields_dict['download_link'].wrapper).html("");
            //                 var _html = '';
            //
            //                 for(var i=0;i<r.message.files.length;i++)
            //                 {
            //                     console.log(r.message.files[i].path);
            //                     _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
            //                         'href="'+r.message.files[i].path+'" download > jpg link </a>' +
				// 						'<script type="text/javascript">' +
            //                         'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'
            //
            //                 }
            //
            //                 console.log(_html);
            //
				// 			 if (cur_frm.fields_dict['download_link']) {
            //                 		$(cur_frm.fields_dict['download_link'].wrapper)
            //                             .html(_html);
            //
				// 			 }
				// 		}
				// 	}
            // });
            //
            // });

            add_summarized = 0;
        }

    }
})