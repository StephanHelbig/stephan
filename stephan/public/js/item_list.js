frappe.listview_settings['Item'] = {
    onload: function(list) {
        list.get_subject_html = function(doc) {
            let user = frappe.session.user;
            let subject_field = this.columns[0].df;
            let value = doc[subject_field.fieldname] || doc.name;
            if (doc.image) {
                var image = (window.cordova && doc.image.indexOf('http')===-1) ?
                frappe.base_url + doc.image : doc.image;
    
                value = repl('<span class="avatar avatar-small" title="%(title)s">\
                    <span class="avatar-frame" style="background-image: url(%(image)s)"\
                    title="%(title)s"></span></span> %(title)s', {
                        image: image,
                        title: value
                    });
            }
            let subject = strip_html(value);
            let escaped_subject = frappe.utils.escape_html(subject);
    
            const liked_by = JSON.parse(doc._liked_by || '[]');
            let heart_class = liked_by.includes(user) ?
                'liked-by' : 'text-extra-muted not-liked';
    
            const seen = JSON.parse(doc._seen || '[]')
                .includes(user) ? '' : 'bold';
    
            let subject_html = `
                <input class="level-item list-row-checkbox hidden-xs" type="checkbox" data-name="${doc.name}">
                <span class="level-item" style="margin-bottom: 1px;">
                    <i class="octicon octicon-heart like-action ${heart_class}"
                        data-name="${doc.name}" data-doctype="${this.doctype}"
                        data-liked-by="${encodeURI(doc._liked_by) || '[]'}"
                    >
                    </i>
                    <span class="likes-count">
                        ${ liked_by.length > 99 ? __("99") + '+' : __(liked_by.length || '')}
                    </span>
                </span>
                <span class="level-item ${seen} ellipsis" title="${escaped_subject}">
                    <a class="ellipsis" href="${this.get_form_link(doc)}" title="${escaped_subject}">
                    ${value}
                    </a>
                </span>
            `;
    
            return subject_html;
        }
    }
}